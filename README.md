# HOWTO

Download scipion app file - *scipion_V1.2.beta_26-12-2017_linux64.tgz*

Set up display number in Dockerfile

```ARG use_display=2```


 Set up DISPLAY variable in the same way

```$ DISPLAY=your_display_number```

Build the image

```$ docker build -t your_image_name .```

Start container

```$ docker run --init --runtime=nvidia --name=your_container_name --rm -i -v /tmp/.X11-unix/X0:/tmp/.X11-unix/X0 -p 590$DISPLAY:590$DISPLAY your_image_name```



